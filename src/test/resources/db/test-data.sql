INSERT INTO vets
VALUES (1, 'James', 'Carter');
INSERT INTO vets
VALUES (2, 'Helen', 'Leary');

INSERT INTO specialties
VALUES (1, 'radiology');
INSERT INTO specialties
VALUES (2, 'surgery');
INSERT INTO specialties
VALUES (3, 'dentistry');

INSERT INTO vet_specialties
VALUES (1, 1);
INSERT INTO vet_specialties
VALUES (1, 2);

INSERT INTO types
VALUES (1, 'cat');
INSERT INTO types
VALUES (2, 'dog');
INSERT INTO types
VALUES (3, 'lizard');
INSERT INTO types
VALUES (4, 'snake');
INSERT INTO types
VALUES (5, 'bird');
INSERT INTO types
VALUES (6, 'hamster');
INSERT INTO types
VALUES (7, 'lion');

INSERT INTO owners
VALUES (1, 'George', 'Franklin', '110 W. Liberty St.', 'Madison', '6085551023');
INSERT INTO owners
VALUES (2, 'Betty', 'Davis', '638 Cardinal Ave.', 'Sun Prairie', '6085551749');
INSERT INTO owners
VALUES (3, 'Ante', 'Radoš', 'Republike Austrije 33', 'Zagreb', '0991113333');

INSERT INTO pets
VALUES (1, 'Leo', '2010-09-07', 1, 1);
INSERT INTO pets
VALUES (2, 'Basil', '2012-08-06', 6, 2);
INSERT INTO pets
VALUES (3, 'Dingo', '2012-08-06', 4, 1);

INSERT INTO visits
VALUES (1, 1, '2013-01-01', 'rabies shot');
INSERT INTO visits
VALUES (3, 2, '2013-01-03', 'neutered');